import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnInit,
  PLATFORM_ID,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {ContactModalComponent} from '../shared/components/contact/contact-modal/contact-modal.component';
import {Pancake, PancakeConfig} from '../shared/components/pancake/pancake.component';
import {FormsService, Review} from '../../services/forms.service';
import {SeoService} from '../../services/seo.service';
import {isPlatformBrowser} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {SwiperOptions} from 'swiper/types';
import TypeIt from "typeit";

interface Services {
  title: string;
  description: string;
  link: string;
}

@Component({
  selector: 'app-home',
  host: {ngSkipHydration: 'true'},
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild('swiper', {static: true}) swiperRef: ElementRef | undefined;
  services: Services[] = [
    {
      title: 'Site Internet',
      description: 'Donnez vie à votre entreprise sur le web.',
      link: '/creation-de-site-internet'
    },
    {
      title: 'App mobile',
      description: 'Offrez une expérience unique à vos utilisateurs.',
      link: '/creation-application-mobile'
    },
    {
      title: 'Solution Cloud',
      description: 'Stockez et partagez vos données en toute sécurité.',
      link: '/solutions-cloud'
    },
  ]
  pancakeItems: Pancake[] = [
    {
      title: 'Vos idées,<br>notre expertise',
      description: 'Vous devez lancer votre entreprise ? Ne négligez pas vos atouts numériques. Donnez la meilleure image de vous-même à travers vos sites internets.',
    },
    {
      title: 'Ne vous donnez aucune limite',
      description:
        'Besoin d\'une application qui associe le web, applications mobiles et les technologies Cloud ? ' +
        'Pas de problème BugProg-Studio relève le défi !',
      buttonText: 'Nous contacter',
    }
  ]
  pancakeConfig: PancakeConfig = {
    imagePath: '/assets/lottie/work.json',
    imgAlt: 'work',
    isLottie: true
  }
  reviews: Review[] = [];

  constructor(
    private dialog: MatDialog,
    private formService: FormsService,
    private seoService: SeoService,
    @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    this.formService.fetchReviews().then((reviews) => {
      reviews.forEach((review: any) => {
        this.reviews.push({
          name: review['name'],
          firstname: review['firstname'],
          website: review['website'],
          role: review['role'],
          textReview: review['textReview'],
          email: review['email'],
          companyName: review['companyName'],
          imagePath: review['imagePath']
        })
      })
    });

    this.seoService.generateTags({
      title: 'Accueil',
      description: 'Boostez votre entreprise avec nos solutions digitales sur mesure. Nous créons des sites web, des applications mobiles et des solutions cloud pour vous aider à atteindre vos objectifs commerciaux. Contactez-nous dès aujourd\'hui pour en savoir plus.'
    });
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      new (TypeIt as any)("#typeIt", {
        speed: 100,
        loop: true
      })
        .type("Site-Web", {delay: 2000})
        .delete(() => {
        }, {delay: 1000, instant: false})
        .type("App  Mobile", {delay: 2000})
        .delete(() => {
        }, {delay: 1000})
        .type("Cloud", {delay: 2000})
        .delete(() => {
        }, {delay: 1000})
        .go();
    }


    const options: SwiperOptions = {
      freeMode: true,
      loop: true,
      centeredSlides: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: true,
        pauseOnMouseEnter: true
      },
      slidesPerView: 1,
      breakpoints: {
        640: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 4,
        },
      }
    }
    Object.assign(this.swiperRef?.nativeElement, options);
  }

  openContactDialog() {
    this.dialog.open(ContactModalComponent, {
      width: '40em',
    });
  }
}
