import {AfterViewInit, Component} from '@angular/core';
import {register} from 'swiper/element/bundle';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements AfterViewInit {

  ngAfterViewInit() {
    register();
  }

}
