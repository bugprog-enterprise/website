import {ContactModalComponent} from '../contact/contact-modal/contact-modal.component';
import {Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DOCUMENT} from '@angular/common';

export interface Pancake {
  title: string;
  description?: string;
  buttonText?: string;
  listItems?: string[]
}

export interface PancakeConfig {
  imagePath: string;
  imgAlt: string;
  isLottie?: boolean;
  reverse?: boolean;
}

@Component({
  selector: 'app-pancake',
  templateUrl: './pancake.component.html',
  host: {ngSkipHydration: 'true'},
  styleUrls: ['./pancake.component.scss']
})
export class PancakeComponent implements OnInit {
  @ViewChild('lottieContainer') lottieEl: ElementRef;
  @Input() pancakes: Pancake[] = [];
  @Input({required: true}) pancakeConfig: PancakeConfig;

  constructor(private dialog: MatDialog, @Inject(DOCUMENT) private document: Document) {
  }

  ngOnInit() {
  }

  openContactDialog() {
    this.dialog.open(ContactModalComponent, {
      width: '40em',
    });
  }
}
