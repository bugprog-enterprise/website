import {Component} from '@angular/core';
import {ContactModalComponent} from '../contact/contact-modal/contact-modal.component';
import {MatDialog} from '@angular/material/dialog';

interface NavigationSections {
  sectionName: string;
  links: {
    text: string,
    hrefAttribute?: string,
    openContactDialog?: boolean,
    routerAttribute?: string
  }[]
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  sections: NavigationSections[] = [
    {
      sectionName: 'Contact',
      links: [
        {
          text: '06 47 50 58 06',
          hrefAttribute: 'tel:+33647505806'
        },
        {
          text: 'contacts@bugprog.studio',
          hrefAttribute: 'mailto:contacts@bugprog.studio'
        },
        {
          text: 'bugprog.studio',
          hrefAttribute: 'https://bugprog.studio'
        },
        {
          text: 'Nous Contacter',
          openContactDialog: true
        },
      ]
    },
    {
      sectionName: 'Navigation',
      links: [
        {
          text: 'Accueil',
          routerAttribute: '/'
        },
        {
          text: 'Site Web',
          routerAttribute: '/creation-de-site-internet'
        },
        {
          text: 'App Mobile',
          routerAttribute: '/creation-application-mobile'
        },
        {
          text: 'Solution Cloud',
          routerAttribute: '/solutions-cloud'
        }
      ]
    },
    {
      sectionName: 'Social',
      links: [
        {
          text: 'Mastodon',
          hrefAttribute: 'https://mastodon.social/@makeElec'
        },
        {
          text: 'Linkedin',
          hrefAttribute: 'https://www.linkedin.com/company/102624181/admin/feed/posts/'
        }
      ]
    },
    {
      sectionName: 'Légal',
      links: [
        {
          text: 'Mentions légales',
          routerAttribute: '/mentions-legales'
        },
        {
          text: 'Qui Sommes Nous ?',
          routerAttribute: '/qui-sommes-nous'
        },
      ]
    }
  ];

  constructor(private dialog: MatDialog) {
  }

  openContactDialog() {
    this.dialog.open(ContactModalComponent, {
      width: '40em',
    });
  }
}
