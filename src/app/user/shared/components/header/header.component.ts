import {AfterViewInit, Component, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterViewInit {

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  ngAfterViewInit() {
    this.document.addEventListener(
      "scroll",
      () => {
        const navBar: HTMLElement = document.getElementById('mainNav')!;
        if (this.document.documentElement.scrollTop === 0) {
          navBar.style.boxShadow = '';
        } else {
          navBar.style.boxShadow = '0 3px 3px rgba(0,0,0,.04)';
        }
      },
      {passive: true}
    );
  }
}
