import {ContactModalComponent} from '../../../shared/components/contact/contact-modal/contact-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {Component} from '@angular/core';

@Component({
  selector: 'app-pricing-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.css']
})
export class WebsitePricingComponent {

  constructor(private dialog: MatDialog) {
  }

  openContactDialog() {
    this.dialog.open(ContactModalComponent, {
      width: '40em',
    });
  }
}
