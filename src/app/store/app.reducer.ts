import * as fromAuth from '../admin/login/store/auth.reducer';
import {ActionReducerMap} from '@ngrx/store';
import {AuthActions} from '../admin/login/store/auth.actions';

export interface AppState {
  auth: fromAuth.State;
}

export const appReducer: ActionReducerMap<AppState, AuthActions> = {
  auth: fromAuth.authReducer
};
