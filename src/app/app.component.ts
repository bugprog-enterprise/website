import {NavigationEnd, Router} from '@angular/router';
import {SeoService} from './services/seo.service';
import {Component, OnInit} from '@angular/core';
import {filter} from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private seoService: SeoService, private router: Router) {
  }

  ngOnInit() {
    this.seoService.createCanonicalLink();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    ).subscribe((val: any) => {
      console.log(val.url)
      this.seoService.updateCanonicalLink(val.url)
    })
  }
}
