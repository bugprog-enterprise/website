import {BrowserModule, provideClientHydration} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ServiceWorkerModule} from '@angular/service-worker';
import {APP_ID, isDevMode, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {UserModule} from './user/user.module';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';

import {SnackbarComponent} from './snackbar/snackbar.component';
import {AppComponent} from './app.component';

import * as fromApp from './store/app.reducer';

import {AuthEffects} from './admin/login/store/auth.effects';

@NgModule({
  declarations: [
    AppComponent,
    SnackbarComponent,
  ],
  imports: [
    UserModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([AuthEffects]),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [
    {provide: APP_ID, useValue: 'server-app'},
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
