import {ContactForm, FormsService} from '../../../services/forms.service';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  forms: ContactForm[] = [];

  constructor(private contactService: FormsService) {
  }

  ngOnInit() {
    this.contactService.fetchContactsForms().then(forms => {
      console.log(forms);
      for (let form of forms.documents) {
        this.forms.push({
          subject: form.subject,
          message: form.message,
          name: form.name,
          firstname: form.firstname,
          phone_number: form.phone_number,
          email: form.email
        })
      }
    })
  }

}
