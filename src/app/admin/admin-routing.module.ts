import {RouterModule, Routes} from '@angular/router';
import {inject, NgModule} from '@angular/core';

import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {AdminComponent} from './admin.component';

import {AuthGuard} from './login/auth.guard';
import {ContactFormComponent} from './dashboard/contact-form/contact-form.component';
import {BannerComponent} from './dashboard/banner/banner.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent, children: [
      {
        path: '', component: DashboardComponent, canActivate: [() => inject(AuthGuard).canActivate()], children: [
          {path: 'contact-form', component: ContactFormComponent},
          {path: 'banner', component: BannerComponent}
        ]
      },
      {path: 'login', component: LoginComponent},
    ]
  },
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
