import {MatFormFieldModule} from '@angular/material/form-field';
import {AdminRoutingModule} from './admin-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';

import {ContactFormComponent} from './dashboard/contact-form/contact-form.component';
import {BannerComponent} from './dashboard/banner/banner.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {AdminComponent} from './admin.component';

@NgModule({
  declarations: [
    AdminComponent,
    LoginComponent,
    BannerComponent,
    DashboardComponent,
    ContactFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    AdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdminModule {
}
