import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Api} from '../../helpers/api';

@Injectable({providedIn: 'root'})
export class AuthGuard {

  constructor(private router: Router) {
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return Api.accounts().get().then(session => {
      if (session && session.email) {
        return true;
      }
      this.router.navigate(['admin', 'login']);
      return false;
    }, () => {
      this.router.navigate(['admin', 'login']);
      return false;
    })
  }
}
