import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as AuthActions from './auth.actions';
import {User} from '../../shared/user.modal';
import {Injectable} from '@angular/core';
import {Api} from '../../../helpers/api';
import {Router} from '@angular/router';
import {switchMap, tap} from 'rxjs';
import {Models} from 'appwrite';

@Injectable()
export class AuthEffects {

  authLogin = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LOGIN_START),
      switchMap((authData: AuthActions.LoginStart) => Api
        .accounts().createEmailSession(authData.payload.email, authData.payload.password)
        .then((res: Models.Session) => {
          const user = new User(res.providerUid, res.userId);
          localStorage.setItem('userData', JSON.stringify(user));
          return new AuthActions.AuthenticateSuccess({
            email: res.providerUid,
            userId: res.userId
          });
        }, (error) => {
          let errorMessage = 'Une erreur c\'est produite';
          switch (error.type) {
            case 'user_invalid_credentials':
              errorMessage = 'Mot de passe invalide';
              break;
            case 'general_rate_limit_exceeded':
              errorMessage = 'Limite de connexion atteinte';
              break;
          }
          return new AuthActions.AuthenticatedFail(errorMessage);
        })
      )
    )
  );

  authSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.AUTHENTICATE_SUCCESS),
      tap(() => {
        this.router.navigate(['/admin', 'contact-form']);
      })
    ), {dispatch: false})

  autologin = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.AUTO_LOGIN),
      switchMap(async () =>
        Api.accounts().get().then(
          session => {
            if (!session || !session.email) {
              return {type: 'DUMMY'};
            }
            return new AuthActions.AuthenticateSuccess({
              email: session.email,
              userId: session.$id
            });
          }, () => {
            return ({type: 'DUMMY'})
          })
      )
    )
  )

  constructor(private actions$: Actions, private router: Router) {
  }
}
