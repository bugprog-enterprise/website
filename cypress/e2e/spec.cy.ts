describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/')
    cy.contains('Libérez le potentiel de votre entreprise avec nos solutions digitales innovantes.')
  })
})
