<?php

function contactFormTemplate($data): string
{

//Mail content
  return "
<!DOCTYPE html>
<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\"
      xmlns:o=\"urn:schemas-microsoft-com:office:office\">
<head>
    <meta content=\"text/html;charset=UTF-8\" http-equiv=\"Content-Type\">
    <title>BugProg Studio</title>
    <!--[if !mso]><!-->
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/>
    <!--<![endif]-->
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
    <style type=\"text/css\">
    #outlook a {
    padding: 0;
        }

body {
    margin: 0;
    padding: 0;
    -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table, td {
    border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
    border: 0;
    height: auto;
    line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
    display: block;
    margin: 0;
}
    </style>
    <!--[if mso]>
    <noscript>
        <xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
    </noscript>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type=\"text/css\">
        .ogf {
    width: 100% !important;
}
    </style>
    <![endif]-->
    <!--[if !mso]><!-->
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:700,400,600,400i\" rel=\"stylesheet\" type=\"text/css\"/>
    <style type=\"text/css\">

    </style>
    <!--<![endif]-->
    <style type=\"text/css\">
    @media only screen and (min-width: 599px) {
    .xc264 {
        width: 264px !important;
                max-width: 264px;
            }

            .xc16 {
        width: 16px !important;
                max-width: 16px;
            }

            .xc272 {
        width: 272px !important;
                max-width: 272px;
            }

            .xc536 {
        width: 536px !important;
                max-width: 536px;
            }

            .pc100 {
        width: 100% !important;
        max-width: 100%;
            }

            .pc97-7551 {
        width: 97.7551% !important;
        max-width: 97.7551%;
            }

            .pc2-2449 {
        width: 2.2449% !important;
        max-width: 2.2449%;
            }

            .xc600 {
        width: 600px !important;
                max-width: 600px;
            }

            .xc568 {
        width: 568px !important;
                max-width: 568px;
            }
        }
    </style>
    <style media=\"screen and (min-width:599px)\">.moz-text-html .xc264 {
    width: 264px !important;
        max-width: 264px;
    }

    .moz-text-html .xc16 {
    width: 16px !important;
        max-width: 16px;
    }

    .moz-text-html .xc272 {
    width: 272px !important;
        max-width: 272px;
    }

    .moz-text-html .xc536 {
    width: 536px !important;
        max-width: 536px;
    }

    .moz-text-html .pc100 {
    width: 100% !important;
    max-width: 100%;
    }

    .moz-text-html .pc97-7551 {
    width: 97.7551% !important;
    max-width: 97.7551%;
    }

    .moz-text-html .pc2-2449 {
    width: 2.2449% !important;
    max-width: 2.2449%;
    }

    .moz-text-html .xc600 {
    width: 600px !important;
        max-width: 600px;
    }

    .moz-text-html .xc568 {
    width: 568px !important;
        max-width: 568px;
    }
    </style>
    <style type=\"text/css\">
    @media only screen and (max-width: 598px) {
    table.fwm {
        width: 100% !important;
    }

            td.fwm {
        width: auto !important;
            }
        }
    </style>
    <style type=\"text/css\">
    u + .emailify .gs {
    background: #000;
    mix-blend-mode: screen;
            display: inline-block;
            padding: 0;
            margin: 0;
        }

        u + .emailify .gd {
    background: #000;
    mix-blend-mode: difference;
            display: inline-block;
            padding: 0;
            margin: 0;
        }

        u + .emailify a, #MessageViewBody a, a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        td.b .klaviyo-image-block {
    display: inline;
    vertical-align: middle;
        }

        @media only screen and (max-width: 599px) {
    .emailify {
        height: 100% !important;
        margin: 0 !important;
                padding: 0 !important;
                width: 100% !important;
            }

            u + .emailify .glist {
        margin-left: 1em !important;
            }

            td.ico.v > div.il > a.l.m, td.ico.v .mn-label {
        padding-right: 0 !important;
                padding-bottom: 16px !important;
            }

            td.x {
        padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .fwm img {
        max-width: 100% !important;
                height: auto !important;
            }

            .aw img {
        width: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            .ah img {
        height: auto !important;
            }

            td.b.nw > table, td.b.nw a {
        width: auto !important;
            }

            td.stk {
        border: 0 !important;
            }

            td.u {
        height: auto !important;
            }

            br.sb {
        display: none !important;
            }

            .thd-1 .i-thumbnail {
        display: inline-block !important;
                height: auto !important;
                overflow: hidden !important;
            }

            .hd-1 {
        display: block !important;
                height: auto !important;
                overflow: visible !important;
            }

            .ht-1 {
        display: table !important;
                height: auto !important;
                overflow: visible !important;
            }

            .hr-1 {
        display: table-row !important;
                height: auto !important;
                overflow: visible !important;
            }

            .hc-1 {
        display: table-cell !important;
                height: auto !important;
                overflow: visible !important;
            }

            div.r.pr-16 > table > tbody > tr > td, div.r.pr-16 > div > table > tbody > tr > td {
        padding-right: 16px !important
            }

            div.r.pl-16 > table > tbody > tr > td, div.r.pl-16 > div > table > tbody > tr > td {
        padding-left: 16px !important
            }

            .hm-1 {
        display: none !important;
                max-width: 0 !important;
                max-height: 0 !important;
                overflow: hidden !important;
                mso-hide: all !important;
            }

            div.h.pt-16 > table > tbody > tr > td {
        padding-top: 16px !important
            }

            div.h.pr-16 > table > tbody > tr > td {
        padding-right: 16px !important
            }

            div.h.pb-16 > table > tbody > tr > td {
        padding-bottom: 16px !important
            }

            div.h.pl-16 > table > tbody > tr > td {
        padding-left: 16px !important
            }

            div.r.pr-40 > table > tbody > tr > td, div.r.pr-40 > div > table > tbody > tr > td {
        padding-right: 40px !important
            }

            div.r.pl-40 > table > tbody > tr > td, div.r.pl-40 > div > table > tbody > tr > td {
        padding-left: 40px !important
            }

            td.x.fs-25 span, td.x.fs-25 > div, td.x.fs-25 {
        font-size: 25px !important
            }

            div.r.pt-0 > table > tbody > tr > td, div.r.pt-0 > div > table > tbody > tr > td {
        padding-top: 0px !important
            }

            div.r.pr-0 > table > tbody > tr > td, div.r.pr-0 > div > table > tbody > tr > td {
        padding-right: 0px !important
            }

            div.r.pb-0 > table > tbody > tr > td, div.r.pb-0 > div > table > tbody > tr > td {
        padding-bottom: 0px !important
            }

            div.r.pl-0 > table > tbody > tr > td, div.r.pl-0 > div > table > tbody > tr > td {
        padding-left: 0px !important
            }

            div.r.pt-15 > table > tbody > tr > td, div.r.pt-15 > div > table > tbody > tr > td {
        padding-top: 15px !important
            }
        }
    </style>
    <meta name=\"color-scheme\" content=\"light dark\"/>
    <meta name=\"supported-color-schemes\" content=\"light dark\"/>
    <!--[if gte mso 9]>
    <style>a:link, span.MsoHyperlink {
    mso-style-priority: 99;
        color: inherit;
        text-decoration: none;
    }

    a:visited, span.MsoHyperlinkFollowed {
    mso-style-priority: 99;
        color: inherit;
        text-decoration: none;
    }
    </style>
    <![endif]-->
    <!--[if gte mso 9]>
    <style>li {
    text-indent: -1em;
    }
    </style>
    <![endif]-->
    <!--[if mso]><!-- -->
    <style>.cr- > table > tbody > tr > td, .c-r > table {
    border-collapse: collapse;
    }
    </style>
    <!--<![endif]-->
</head>
<body lang=\"en\" link=\"#DD0000\" vlink=\"#DD0000\" class=\"emailify\"
      style=\"mso-line-height-rule:exactly;word-spacing:normal;background-color:#e3e9f4;\">
<div style=\"display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0;max-width:0;opacity:0;overflow:hidden;\">
    Quoi de neuf ?
</div>
<div class=\"bg\" style=\"background-color:#e3e9f4;\" lang=\"en\">
    <!--[if mso | IE]>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-16-outlook pl-16-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-16 pl-16\" style=\"background:transparent;margin:0px auto;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:transparent;width:100%;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:10px 32px 10px 32px;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"m-outlook c-outlook -outlook hm-1-outlook -outlook\"
                                style=\"vertical-align:top;width:264px;\">
                    <![endif]-->
                    <div class=\"xc264 ogf m c  hm-1\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border:none;vertical-align:top;\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td class=\"s\" style=\"font-size:0;padding:0;word-break:break-word;\">
                                    <div style=\"height:4px;line-height:4px;\"> </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    <td class=\"g-outlook\" style=\"vertical-align:top;width:16px;\">
                    <![endif]-->
                    <div class=\"xc16 ogf g\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td style=\"vertical-align:top;padding:0;\">
                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\" width=\"100%\">
                                        <tbody></tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    <td class=\"c-outlook -outlook -outlook\" style=\"vertical-align:top;width:264px;\">
                    <![endif]-->
                    <div class=\"xc264 ogf c\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border:none;vertical-align:top;\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td class=\"s\" style=\"font-size:0;padding:0;word-break:break-word;\">
                                    <div style=\"height:4px;line-height:4px;\"> </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    <td class=\"\" style=\"vertical-align:top;width:272px;\">
                    <![endif]-->
                    <div class=\"xc272 ogf\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td style=\"vertical-align:top;padding:0;\">
                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\" width=\"100%\">
                                        <tbody></tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
                <v:image
                        style=\"border:0;height:70px;mso-position-horizontal:center;position:absolute;top:0;width:600px;z-index:-3;\"
                        src=\"https://e.hypermatic.com/ec1b37521802cc61c1ba764bcd815577.png\"
                        xmlns:v=\"urn:schemas-microsoft-com:vml\"/>
                <v:rect style=\"border:0;height:70px;mso-position-horizontal:center;position:absolute;top:0;width:600px;z-index:-4;\"
                        fillcolor=\"#000000\" stroke=\"false\" strokecolor=\"none\" strokeweight=\"0\"
                        xmlns:v=\"urn:schemas-microsoft-com:vml\"/>
    <![endif]-->
    <div class=\"h  pt-16 pr-16 pb-16 pl-16\" style=\"margin:0 auto;max-width:600px;\">
        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"width:100%;\">
            <tbody>
            <tr style=\"vertical-align:top;\">
                <td background=\"https://img-cache.net/im/4738519/15b4055bcf05b68e9786fe0e2225d3a8958e907d950d5de2a8ed1b063a645651.png?e=uaPv1xtphkRgCmXdKXy3pkppnGwVXWpAQXD5Q_ru00hwSE407badMxkEF9nfWacVFkVLpqU5PMY9LplR4nFiI84s7J_jgiEEFBjauA_aA9ijIM4F-zR4__mel6qrK9aQGFMaRDNIpsg_RSmxh3wB0Qe94oQ5yYAFEHax8zV2uKkZXJUsvBcIwli9-oc\"
                    style=\"background-image:linear-gradient(20deg, #65a3ff, #5238df); background-size: cover;background-position:center center;background-repeat:no-repeat;border-radius:20px 20px 20px 20px;padding:20px 50px 20px 50px;vertical-align:top;height:30px;\"
                    height=\"30\" sib_link_id=\"10\">
                    <!--[if mso | IE]>
                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:600px;\" width=\"600\">
                        <tr>
                            <td style=\"padding:20px 50px 20px 50px;\">
                    <![endif]-->
                    <div class=\"mj-hero-content\" style=\"margin:0px auto;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"width:100%;margin:0;\">
                            <tbody>
                            <tr>
                                <td style=\"\">
                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                                           style=\"width:100%;margin:0;\">
                                        <tbody>
                                        <tr>
                                            <td align=\"center\" class=\"i\"
                                                style=\"font-size:0;padding:0;word-break:break-word;\">
                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                                                       style=\"border-collapse:collapse;border-spacing:0;\">
                                                    <tbody>
                                                    <tr>
                                                        <td style=\"width:300px;\"><img alt=\"\"
                                                                                     src=\"https://bugprog.studio/assets/images/banner_white.webp\"
                                                                                     style=\"border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;\"
                                                                                     title=\"\" width=\"400\" height=\"auto\"
                                                                                     sib_link_id=\"11\"/>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-16-outlook pl-16-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-16 pl-16\" style=\"background:transparent;margin:0px auto;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:transparent;width:100%;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:10px 32px 10px 32px;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"c-outlook -outlook -outlook\" style=\"vertical-align:middle;width:536px;\">
                    <![endif]-->
                    <div class=\"xc536 ogf c\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border:none;vertical-align:middle;\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td class=\"s\" style=\"font-size:0;padding:0;word-break:break-word;\">
                                    <div style=\"height:16px;line-height:16px;\"> </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-40-outlook pl-40-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-40 pl-40\"
         style=\"background:#fffffe;background-color:#fffffe;margin:0px auto;border-radius:20px 20px 20px 20px;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:#fffffe;background-color:#fffffe;width:100%;border-radius:20px 20px 20px 20px;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:30px 55px 30px 55px;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"\" style=\"width:490px;\">
                    <![endif]-->
                    <div class=\"pc100 ogf\"
                         style=\"font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;\">
                        <!--[if mso | IE]>
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\">
                            <tr>
                                <td style=\"vertical-align:middle;width:478px;\">
                        <![endif]-->
                        <div class=\"pc97-7551 ogf c\"
                             style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:97.7551%;\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">
                                <tbody>
                                <tr>
                                    <td style=\"border:none;vertical-align:middle;padding:8px 0px 8px 0px;\">
                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\"
                                               width=\"100%\">
                                            <tbody>
                                            <tr>
                                                <td align=\"left\" class=\"x  fs-25 m\"
                                                    style=\"font-size:0;padding-bottom:14px;word-break:break-word;\">
                                                    <div style=\"text-align:left;\"><p
                                                            style=\"Margin:0;text-align:left;mso-line-height-alt:100%\">
                                                        <span style=\"font-size:28px;font-family:Montserrat,Arial,sans-serif;font-weight:700;color:#0b0c0f;line-height:100%;\">Hey</span><span
                                                            style=\"font-size:28px;font-family:Montserrat,Arial,sans-serif;font-weight:700;color:#5238df;line-height:100%;\"> </span><span
                                                            style=\"font-size:28px;font-family:Montserrat,Arial,sans-serif;font-weight:700;color:#5238df;line-height:100%;\">{$data['firstname']},</span>
                                                    </p></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align=\"left\" class=\"x\"
                                                    style=\"font-size:0;padding-bottom:0;word-break:break-word;\">
                                                    <div style=\"text-align:left;\">
                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span
                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\">
                                                                    Nous vous remercions d'avoir pris le temps de nous contacter via notre formulaire en ligne. Nous avons bien reçu votre message et nous vous en remercions.
                                                          <br>
                                                          <br>

Nous tenons à vous assurer que nous accordons une grande importance à toutes les demandes que nous recevons de nos clients. C'est pourquoi, nous allons étudier votre demande avec le plus grand soin et nous vous répondrons dans les plus brefs délais.
                                                        </span>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if mso | IE]>
                        </td>
                        <td style=\"vertical-align:top;width:11px;\">
                        <![endif]-->
                        <div class=\"pc2-2449 ogf\"
                             style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:2.2449%;\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">
                                <tbody>
                                <tr>
                                    <td style=\"vertical-align:top;padding:0;\">
                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\"
                                               width=\"100%\">
                                            <tbody></tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-16-outlook pl-16-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-16 pl-16\" style=\"background:transparent;margin:0px auto;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:transparent;width:100%;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:10px 32px 10px 32px;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"c-outlook -outlook -outlook\" style=\"vertical-align:middle;width:536px;\">
                    <![endif]-->
                    <div class=\"xc536 ogf c\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border:none;vertical-align:middle;\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td class=\"s\" style=\"font-size:0;padding:0;word-break:break-word;\">
                                    <div style=\"height:16px;line-height:16px;\"> </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pt-0-outlook pr-0-outlook pb-0-outlook pl-0-outlook -outlook\" role=\"none\"
           style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pt-0 pr-0 pb-0 pl-0\"
         style=\"background:transparent;margin:0px auto;border-radius:20px 20px 20px 20px;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:transparent;width:100%;border-radius:20px 20px 20px 20px;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:0;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"c-outlook -outlook cr--outlook -outlook\"
                                style=\"vertical-align:middle;width:600px;\">
                    <![endif]-->
                    <div class=\"xc600 ogf c  cr-\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border-collapse:separate;border-spacing:0;border:none;border-radius:20px 20px 20px 20px;vertical-align:middle;\"
                               width=\"100%\" bgcolor=\"transparent\" valign=\"middle\">
                            <tbody>
                            <tr>
                                <td align=\"center\" class=\"i  fw-1\"
                                    style=\"border-collapse:separate;border-spacing:0;font-size:0;padding:0;word-break:break-word;\">
                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                                           style=\"border-collapse:collapse;border-spacing:0;\" class=\"fwm\">
                                        <tbody>
                                        <tr>
                                            <td style=\"width:600px;\" class=\"fwm\"><a
                                                    href=\"https://86z8l.r.a.d.sendibm1.com/mk/cl/f/WiR9Cyx3fziSO_7nT6XGV3CAGiHNdCTQRY0LT2xIG3EjS2Gw0EmqV0CtADsT4HhwyymSztWdzy6nmrYps3EDVQHLFZ70M_D8od7VDjcPH3JDYQc0vWEt31FI-w_ZcOwkR5QfI1QV-K1sonic46E5lJ4Kf8tSnEnQD2U-jODhdU5KyR6oIKaswWozAW_mIAlwBLTi-JxvpygjIuvfTakONnaP3EBeeS7gzO4ivUkuxclNzUpSOZdKoeBKnr_WtWDQYs7FtteLcf_rABXP0vpZQWc1B-x7UWCXFv6N3ut_41dhZTr0mUOq8Cp5dydJjW33tGdguXoVkBp-tt8rA0kCx-8eebhBU5QvyfO2y8sNf7-rFw8HqgFo3q3NqRtKC2TJ9evM5fGe9CuH4seLTHvDu42E-8AP\"
                                                    target=\"_blank\" title=\"\" sib_link_id=\"1\"> <img alt=\"\"
                                                                                                   src=\"https://lh3.ggpht.com/p/AF1QipNUo07CRsdUHodMbPQFLGoPdKEvPHSfCsHsvWj1=s1536\"
                                                                                                   style=\"border:0;border-radius:20px 20px 20px 20px;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;\"
                                                                                                   title=\"\" width=\"600\"
                                                                                                   height=\"auto\"
                                                                                                   sib_link_id=\"12\"/></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-16-outlook pl-16-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-16 pl-16\" style=\"background:transparent;margin:0px auto;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:transparent;width:100%;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:10px 32px 10px 32px;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"c-outlook -outlook -outlook\" style=\"vertical-align:middle;width:536px;\">
                    <![endif]-->
                    <div class=\"xc536 ogf c\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border:none;vertical-align:middle;\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td class=\"s\" style=\"font-size:0;padding:0;word-break:break-word;\">
                                    <div style=\"height:16px;line-height:16px;\"> </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-40-outlook pl-40-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-40 pl-40\"
         style=\"background:#fffffe;background-color:#fffffe;margin:0px auto;border-radius:20px 20px 20px 20px;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:#fffffe;background-color:#fffffe;width:100%;border-radius:20px 20px 20px 20px;\">
            <tbody>
<!--            <tr>-->
<!--                <td style=\"border:none;direction:ltr;font-size:0;padding:30px 55px 30px 55px;text-align:left;\">-->
<!--                    &lt;!&ndash;[if mso | IE]>-->
<!--                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">-->
<!--                        <tr>-->
<!--                            <td class=\"\" style=\"width:490px;\">-->
<!--                    <![endif]&ndash;&gt;-->
<!--                    <div class=\"pc100 ogf\"-->
<!--                         style=\"font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;\">-->
<!--                        &lt;!&ndash;[if mso | IE]>-->
<!--                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\">-->
<!--                            <tr>-->
<!--                                <td style=\"vertical-align:middle;width:478px;\">-->
<!--                        <![endif]&ndash;&gt;-->
<!--                        <div class=\"pc97-7551 ogf c\"-->
<!--                             style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:97.7551%;\">-->
<!--                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">-->
<!--                                <tbody>-->
<!--                                <tr>-->
<!--                                    <td style=\"border:none;vertical-align:middle;padding:8px 0px 8px 0px;\">-->
<!--                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\"-->
<!--                                               width=\"100%\">-->
<!--                                            <tbody>-->
<!--                                            <tr>-->
<!--                                                <td align=\"left\" class=\"x\" style=\"font-size:0;word-break:break-word;\">-->
<!--                                                    <div style=\"text-align:left;\"><p-->
<!--                                                            style=\"Margin:0;text-align:left;mso-line-height-alt:131%\">-->
<!--                                                        <span style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\">Nos offres :</span>-->
<!--                                                    </p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> </span>-->
<!--                                                        </p>-->
<!--                                                        <ol class=\"glist\"-->
<!--                                                            style=\"font-size:13px;color:#0b0c0f;padding:0;font-weight:600;Margin:0 0 0 18px;\">-->
<!--                                                            <li style=\"Margin:0;\"><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:600;color:#0b0c0f;line-height:131%;\"></span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:600;color:#0b0c0f;line-height:131%;\">Augmentation de la limite pour les partages publics</span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> : on travaille à prolonger la durée de vie des partages publics, actuellement de 7 jours. Restez connectés pour savoir combien de jours supplémentaires vous serons proposés !</span>-->
<!--                                                            </li>-->
<!--                                                            <li style=\"Margin:0;\"><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"></span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:600;color:#0b0c0f;line-height:131%;\">Historique des versions de fichiers</span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> : vous pourrez bientôt naviguer dans l&#39;historique des modifications d&#39;un même document. Vous modifiez un fichier et voulez revenir en arrière ? Grâce à l&#39;historique, vous pourrez revenir à une version antérieure.</span>-->
<!--                                                            </li>-->
<!--                                                            <li style=\"Margin:0;\"><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"></span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:600;color:#0b0c0f;line-height:131%;\">Migration des données depuis Google Drive &amp; Dropbox</span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> : on développe un outil pour rapatrier facilement le contenu stocké sur des services concurrents. De quoi faciliter votre transition vers Shadow Drive et dire adieu à vos anciens services de stockage !</span>-->
<!--                                                            </li>-->
<!--                                                            <li style=\"Margin:0;\"><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"></span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:600;color:#0b0c0f;line-height:131%;\">Mises à jour de sécurité et de performances</span><span-->
<!--                                                                    style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> : On prévoit deux mises à jour, la v25.0.7 et la v26.0 en provenance de notre partenaire Nextcloud. Leur but ? Assurer une meilleure sécurité et des performances optimales pour votre utilisation de Shadow Drive.</span>-->
<!--                                                            </li>-->
<!--                                                        </ol>-->
<!--                                                        <p></p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> </span>-->
<!--                                                        </p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\">On travaille d&#39;arrache-pied pour que Shadow Drive devienne votre meilleur allié en matière de stockage en ligne. </span>-->
<!--                                                        </p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> </span>-->
<!--                                                        </p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\">Merci de nous faire confiance dans cette aventure !</span>-->
<!--                                                        </p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\"> </span>-->
<!--                                                        </p>-->
<!--                                                        <p style=\"Margin:0;mso-line-height-alt:131%\"><span-->
<!--                                                                style=\"font-size:13px;font-family:Montserrat,Arial,sans-serif;font-weight:400;color:#0b0c0f;line-height:131%;\">À très vite,<br-->
<!--                                                                class=\"sb\"/><br-->
<!--                                                                class=\"sb\"/>BugProg Studio</span></p></div>-->
<!--                                                </td>-->
<!--                                            </tr>-->
<!--                                            </tbody>-->
<!--                                        </table>-->
<!--                                    </td>-->
<!--                                </tr>-->
<!--                                </tbody>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                        &lt;!&ndash;[if mso | IE]>-->
<!--                        </td>-->
<!--                        <td style=\"vertical-align:top;width:11px;\">-->
<!--                        <![endif]&ndash;&gt;-->
<!--                        <div class=\"pc2-2449 ogf\"-->
<!--                             style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:2.2449%;\">-->
<!--                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">-->
<!--                                <tbody>-->
<!--                                <tr>-->
<!--                                    <td style=\"vertical-align:top;padding:0;\">-->
<!--                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\"-->
<!--                                               width=\"100%\">-->
<!--                                            <tbody></tbody>-->
<!--                                        </table>-->
<!--                                    </td>-->
<!--                                </tr>-->
<!--                                </tbody>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                        &lt;!&ndash;[if mso | IE]>-->
<!--                        </td></tr></table>-->
<!--                        <![endif]&ndash;&gt;</div>-->
<!--                    &lt;!&ndash;[if mso | IE]>-->
<!--                    </td></tr></table>-->
<!--                    <![endif]&ndash;&gt;-->
<!--                </td>-->
<!--            </tr>-->
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pr-16-outlook pl-16-outlook -outlook\" role=\"none\" style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
    <![endif]-->
    <div class=\"r  pr-16 pl-16\" style=\"background:transparent;margin:0px auto;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
               style=\"background:transparent;width:100%;\">
            <tbody>
            <tr>
                <td style=\"border:none;direction:ltr;font-size:0;padding:10px 32px 10px 32px;text-align:left;\">
                    <!--[if mso | IE]>
                    <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                            <td class=\"c-outlook -outlook -outlook\" style=\"vertical-align:middle;width:536px;\">
                    <![endif]-->
                    <div class=\"xc536 ogf c\"
                         style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;\">
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                               style=\"border:none;vertical-align:middle;\" width=\"100%\">
                            <tbody>
                            <tr>
                                <td class=\"s\" style=\"font-size:0;padding:0;word-break:break-word;\">
                                    <div style=\"height:16px;line-height:16px;\"> </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
           class=\"r-outlook -outlook pt-15-outlook pr-16-outlook pl-16-outlook -outlook\" role=\"none\"
           style=\"width:600px;\" width=\"600\">
        <tr>
            <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"width:600px;\"
                       width=\"600\">
                    <tr>
                        <td style=\"line-height:0;font-size:0;mso-line-height-rule:exactly;\">
                            <v:image
                                    style=\"border:0;height:86px;mso-position-horizontal:center;position:absolute;top:0;width:600px;z-index:-3;\"
                                    src=\"https://e.hypermatic.com/a4ff90aaec00a9cab0a31adeaa398386.png\"
                                    xmlns:v=\"urn:schemas-microsoft-com:vml\"/>
                            <v:rect style=\"border:0;height:86px;mso-position-horizontal:center;position:absolute;top:0;bottom:0;width:600px;z-index:-4;\"
                                    fillcolor=\"transparent\" stroke=\"false\" strokecolor=\"none\" strokeweight=\"0\"
                                    xmlns:v=\"urn:schemas-microsoft-com:vml\"/>
    <![endif]-->
    <div class=\"r  pt-15 pr-16 pl-16\"
         style=\"background-image:linear-gradient(20deg, #65a3ff, #5238df);background-repeat:no-repeat;background-size:cover;margin:0px auto;border-radius:20px 20px 20px 20px;max-width:600px;\">
        <div style=\"line-height:0;font-size:0;\">
            <table align=\"center\"
                   background=\"https://img-cache.net/im/4738519/59a3c8bfaba98d18e61bd21bff39d68c5ca4738401c38f022fb811801255d994.png?e=tK3vGkSDOCRRWhE06sKak1D_ncvswfDL9cT28zR9s1w4OANbYkSJR8Vjgy6X6Hkh0Lzsdi5gdSRkfL5TpZ40qDoW32G9QV0oyVGBEYErcOzq7hjZyfXp057Oz6rmr14ewbPHNFUD9i9UZ-An7daDEP8LWriEECi50WdHiiloWoinqiUUOHBxYDEXCfc\"
                   border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\"
                   style=\"background-image:linear-gradient(20deg, #65a3ff, #5238df);background-position:center center;background-repeat:no-repeat;background-size:cover;width:100%;border-radius:20px 20px 20px 20px;\"
                   sib_link_id=\"13\">
                <tbody>
                <tr>
                    <td style=\"border:none;direction:ltr;font-size:0;padding:16px 16px 16px 16px;text-align:center;\">
                        <!--[if mso | IE]>
                        <table role=\"none\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                            <tr>
                                <td class=\"c-outlook -outlook -outlook\" style=\"vertical-align:middle;width:568px;\">
                        <![endif]-->
                        <div class=\"xc568 ogf c\"
                             style=\"font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" width=\"100%\">
                                <tbody>
                                <tr>
                                    <td style=\"border:none;vertical-align:middle;padding:10px 0px 10px 0px;\">
                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"none\" style=\"\"
                                               width=\"100%\">
                                            <tbody>
                                            <tr>
                                                <td align=\"center\" class=\"o\"
                                                    style=\"font-size:0;padding:0;word-break:break-word;\">
                                                    <!--[if mso | IE]>
                                                    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                           role=\"none\">
                                                        <tr>
                                                            <td>
                                                    <![endif]-->
                                                    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                           role=\"none\" style=\"float:none;display:inline-table;\">
                                                        <tbody>
                                                        <tr class=\"e  m\">
                                                            <td style=\"padding:0 16px 0 0;vertical-align:middle;\">
                                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                                       role=\"none\" style=\"width:20px;\">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style=\"font-size:0;height:20px;vertical-align:middle;width:20px;\">
                                                                            <a href=\"https://twitter.com/BugProg_Nantes\"
                                                                               target=\"_blank\" sib_link_id=\"2\">         <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"30\" height=\"30\" fill=\"white\" class=\"bi bi-twitter\" viewBox=\"0 0 16 16\">
                                                                              <path d=\"M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z\"/>
                                                                            </svg></a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso | IE]>
                                                    </td>
                                                    <td>
                                                    <![endif]-->
                                                    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                           role=\"none\" style=\"float:none;display:inline-table;\">
                                                        <tbody>
                                                        <tr class=\"e  m\">
                                                            <td style=\"padding:0 16px 0 0;vertical-align:middle;\">
                                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                                       role=\"none\" style=\"width:20px;\">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style=\"font-size:0;height:20px;vertical-align:middle;width:20px;\">
                                                                            <a href=\"https://mastodon.social/@makeElec\"
                                                                               target=\"_blank\" sib_link_id=\"3\">         <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"30\" height=\"30\" fill=\"white\" class=\"bi bi-mastodon\" viewBox=\"0 0 16 16\">
                                                                              <path d=\"M11.19 12.195c2.016-.24 3.77-1.475 3.99-2.603.348-1.778.32-4.339.32-4.339 0-3.47-2.286-4.488-2.286-4.488C12.062.238 10.083.017 8.027 0h-.05C5.92.017 3.942.238 2.79.765c0 0-2.285 1.017-2.285 4.488l-.002.662c-.004.64-.007 1.35.011 2.091.083 3.394.626 6.74 3.78 7.57 1.454.383 2.703.463 3.709.408 1.823-.1 2.847-.647 2.847-.647l-.06-1.317s-1.303.41-2.767.36c-1.45-.05-2.98-.156-3.215-1.928a3.614 3.614 0 0 1-.033-.496s1.424.346 3.228.428c1.103.05 2.137-.064 3.188-.189zm1.613-2.47H11.13v-4.08c0-.859-.364-1.295-1.091-1.295-.804 0-1.207.517-1.207 1.541v2.233H7.168V5.89c0-1.024-.403-1.541-1.207-1.541-.727 0-1.091.436-1.091 1.296v4.079H3.197V5.522c0-.859.22-1.541.66-2.046.456-.505 1.052-.764 1.793-.764.856 0 1.504.328 1.933.983L8 4.39l.417-.695c.429-.655 1.077-.983 1.934-.983.74 0 1.336.259 1.791.764.442.505.661 1.187.661 2.046v4.203z\"/>
                                                                            </svg></a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso | IE]>
                                                    </td>
                                                    <td>
                                                    <![endif]-->
                                                    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                           role=\"none\" style=\"float:none;display:inline-table;\">
                                                        <tbody>
                                                        <tr class=\"e  \">
                                                            <td style=\"padding:0;padding-right:0;vertical-align:middle;\">
                                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"
                                                                       role=\"none\" style=\"width:20px;\">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style=\"font-size:0;height:20px;vertical-align:middle;width:20px;\">
                                                                            <a href=\"https://bugprog.studio\"
                                                                               target=\"_blank\" sib_link_id=\"4\"> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"30\" height=\"30\" fill=\"white\" class=\"bi bi-browser-firefox\" viewBox=\"0 0 16 16\">
                                                                              <path d=\"M13.384 3.408c.535.276 1.22 1.152 1.556 1.963a7.98 7.98 0 0 1 .503 3.897l-.009.077a8.533 8.533 0 0 1-.026.224A7.758 7.758 0 0 1 .006 8.257v-.04c.016-.363.055-.724.114-1.082.01-.074.075-.42.09-.489l.01-.051a6.551 6.551 0 0 1 1.041-2.35c.217-.31.46-.6.725-.87.233-.238.487-.456.758-.65a1.5 1.5 0 0 1 .26-.137c-.018.268-.04 1.553.268 1.943h.003a5.744 5.744 0 0 1 1.868-1.443 3.597 3.597 0 0 0 .021 1.896c.07.047.137.098.2.152.107.09.226.207.454.433l.068.066.009.009a1.933 1.933 0 0 0 .213.18c.383.287.943.563 1.306.741.201.1.342.168.359.193l.004.008c-.012.193-.695.858-.933.858-2.206 0-2.564 1.335-2.564 1.335.087.997.714 1.839 1.517 2.357a3.72 3.72 0 0 0 .439.241c.076.034.152.065.228.094.325.115.665.18 1.01.194 3.043.143 4.155-2.804 3.129-4.745v-.001a3.005 3.005 0 0 0-.731-.9 2.945 2.945 0 0 0-.571-.37l-.003-.002a2.679 2.679 0 0 1 1.87.454 3.915 3.915 0 0 0-3.396-1.983c-.078 0-.153.005-.23.01l-.042.003V4.31h-.002a3.882 3.882 0 0 0-.8.14 6.454 6.454 0 0 0-.333-.314 2.321 2.321 0 0 0-.2-.152 3.594 3.594 0 0 1-.088-.383 4.88 4.88 0 0 1 1.352-.289l.05-.003c.052-.004.125-.01.205-.012C7.996 2.212 8.733.843 10.17.002l-.003.005.003-.001.002-.002h.002l.002-.002a.028.028 0 0 1 .015 0 .02.02 0 0 1 .012.007 2.408 2.408 0 0 0 .206.48c.06.103.122.2.183.297.49.774 1.023 1.379 1.543 1.968.771.874 1.512 1.715 2.036 3.02l-.001-.013a8.06 8.06 0 0 0-.786-2.353Z\"/>
                                                                            </svg></a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso | IE]>
                                                    </td></tr></table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!--[if mso | IE]>
    </td></tr></table>
    </td></tr></table>
    <![endif]--></div>
</body>
</html>

";
}

?>
